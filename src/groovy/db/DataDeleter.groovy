package db;

import assignment.Skill;
import assignment.Person;
import assignment.Task
import assignment.*;


class DataDeleter{

		
	def deleteAll = {
		deletePersons();
		deleteTasks();
		deleteSkills();
		deletePeriods();
	}

	def deletePersons = {
		Person.list().each {
			it.delete();
			println("person ${it.toString()} deleted" )
		}
	}
	
	def deleteTasks = {
		Task.list().each {
			it.delete();
			println("task ${it.toString()} deleted" )
		}
	}

	def deleteSkills = {
		Skill.list().each {
			it.delete();
			println("skill ${it.toString()} deleted" )
		}
	}
	
	def deletePeriods = {
		Period.list().each {
			it.delete();
			println("period ${it.toString()} deleted" )
		}
	}
	
}
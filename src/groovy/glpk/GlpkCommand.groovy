package glpk

import app.Constants;

import app.*;
import assignment.Person;

class GlpkCommand {

	private String doubleQuote = Constants.doubleQuote;
	private String lineSeparatorGLPK = Constants.lineSeparatorGLPK;
	private String lineSeparator = Constants.lineSeparator;
	private String personSet = Constants.personSetName;
	private String taskSet = Constants.taskSetName;
	private String skillSet = Constants.skillSetName;
	private String periodSet = Constants.periodSetName;
	private String personIndex = Constants.personSetIndex;
	private String taskIndex = Constants.taskSetIndex;
	private String skillIndex = Constants.skillSetIndex;
	private String periodIndex = Constants.periodSetIndex;

	public String toString() {
		String command = 'solve;' + lineSeparator;

		//command += "display obj, x > ${doubleQuote}./solver/res.txt;${doubleQuote}" + lineSeparator;


		//Glpk display command
		//Problem displaying {i in I} with i as text

		//for {p in PERSON, t in TASK,a in PERIOD} {printf "%d;%d;%d;%d; \n", p,t,a, x[p,t,a];}

//Header
//printf $doubleQuote${personSet};${taskSet};${periodSet};${Constants.variableName}[${personIndex},${taskIndex},${periodIndex}];$lineSeparatorGLPK$doubleQuote;

		command +="""
for {${personIndex} in ${personSet}, ${taskIndex} in ${taskSet}, ${periodIndex} in ${periodSet}}  {printf $doubleQuote%d;%d;%d;%d;$lineSeparatorGLPK$doubleQuote, ${personIndex},${taskIndex},${periodIndex},${Constants.variableName}[${personIndex},${taskIndex},${periodIndex}];}
""";

		//command +="display x;";
		/*		
		 command +="""
		 printf $doubleQuote $lineSeparatorGLPK $doubleQuote;
		 printf $doubleQuote Agent  Task $lineSeparatorGLPK $doubleQuote;
		 for {$personIndex in $personSet}
		 {
		 printf $doubleQuote %5d %5d $doubleQuote, $personIndex ;
		 printf {$taskIndex in $taskSet: x[$personIndex,$taskIndex]} $doubleQuote %-10s $doubleQuote,$taskIndex;
		 printf $doubleQuote $lineSeparatorGLPK $doubleQuote ;
		 }
		 printf $doubleQuote $lineSeparatorGLPK $doubleQuote;"""
		 */

		return command;
	}
}

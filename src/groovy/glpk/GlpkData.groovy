package glpk

import app.*;
import assignment.*;

class GlpkData {

	GlpkModel gm = new GlpkModel();

	public String toString() {
		String data = 'data;'+ Constants.lineSeparator;

		//parametres

		data += getPersonSkillMatrix()+ Constants.lineSeparator;
		data += getTaskSkillMatrix()+ Constants.lineSeparator;

		data += getPersonPeriodMatrix()+ Constants.lineSeparator;
		data += getTaskPeriodMatrix()+ Constants.lineSeparator;

		data += 'end;'+ Constants.lineSeparator;
		return data;
	}


	public String getPersonSkillMatrix() {
		String result="""
param personSkills : 
${Constants.space}${Constants.space}${Constants.space}${gm.getSkillList('', Constants.space)} :=""";
		try {
			Person.list().each { result += "${Constants.lineSeparator} ${it.toString()} ${getSkillsFromPerson(it)}" }
		}
		catch(Exception e) {
			println("GlpkData.getPersonSkillMatrix() : " + e.getMessage());
		}
		finally {
			println("GlpkData.getPersonSkillMatrix() : $result");
			return result + ";" + Constants.lineSeparator;
		}
	}

	public String getSkillsFromPerson(Person p) {
		String result = "";
		def SkillsList = Skill.list().sort{};
		def personSkills = p.getSkills();
		def test;

		for(s in SkillsList) {

			try{
				test = personSkills.find { it.equals(s) }
			}
			catch(Exception e){
				println("GlpkData.getSkillsFromPerson() ${p.username}}: " + e.getMessage());
			}
			finally{
				println("person: ${p.username} / s: ${s.id}/ test: ${test} ");

				if(test) {
					result += "1 ";
				}
				else {
					result += "0 ";
				}
			}
		}
		return result;
	}

	public String getTaskSkillMatrix() {
		String result="""
param taskSkills : 
${Constants.space}${Constants.space}${Constants.space}${gm.getSkillList('', Constants.space)} :=""";
		try {
			Task.list().each { result += "${Constants.lineSeparator} ${it.toString()} ${getSkillsFromTask(it)}" }
		}
		catch(Exception e) {
			println("GlpkData.getTaskSkillMatrix() : " + e.getMessage());
		}
		finally {
			println("GlpkData.getTaskSkillMatrix() : $result");
			return result + ";" + Constants.lineSeparator;
		}
	}

	public String getSkillsFromTask(Task t) {
		String result = "";
		def SkillsList = Skill.list().sort{};
		def taskSkills = t.getSkills();
		def test;

		for(s in SkillsList) {

			try{
				test = taskSkills.find { it.equals(s) }
			}
			catch(Exception e){
				println("GlpkData.getSkillsFromTask() ${t.name}}: " + e.getMessage());
			}
			finally{
				println("task: ${t.name} / s: ${s.id}/ test: ${test} ");

				if(test) {
					result += "1 ";
				}
				else {
					result += "0 ";
				}
			}
		}
		return result;
	}


	public String getPersonPeriodMatrix() {
		String result="""
param personPeriods : 
${Constants.space}${Constants.space}${Constants.space}${gm.getPeriodList('', Constants.space)} :=""";
		try {
			Person.list().each { result += "${Constants.lineSeparator} ${it.toString()} ${getPeriodsFromPerson(it)}" }
		}
		catch(Exception e) {
			println("GlpkData.getPersonPeriodMatrix() : " + e.getMessage());
		}
		finally {
			println("GlpkData.getPersonPeriodMatrix() : $result");
			return result + ";" + Constants.lineSeparator;
		}
	}

	public String getPeriodsFromPerson(Person p) {
		String result = "";
		def PeriodsList = Period.list().sort{};
		def personPeriods = p.getPeriods();
		def test;

		for(s in PeriodsList) {

			try{
				test = personPeriods.find { it.equals(s) }
			}
			catch(Exception e){
				println("GlpkData.getPeriodsFromPerson() ${p.username}}: " + e.getMessage());
			}
			finally{
				println("person: ${p.username} / period: ${s.id}/ test: ${test} ");

				if(test) {
					result += "1 ";
				}
				else {
					result += "0 ";
				}
			}
		}
		return result;
	}


	public String getTaskPeriodMatrix() {
		String result="""
param taskPeriods : 
${Constants.space}${Constants.space}${Constants.space}${gm.getPeriodList('', Constants.space)} :=""";
		try {
			Task.list().each { result += "${Constants.lineSeparator} ${it.toString()} ${getPeriodsFromTask(it)}" }
		}
		catch(Exception e) {
			println("GlpkData.getTaskPeriodMatrix() : " + e.getMessage());
		}
		finally {
			println("GlpkData.getTaskPeriodMatrix() : $result");
			return result + ";" + Constants.lineSeparator;
		}
	}

	public String getPeriodsFromTask(Task t) {
		String result = "";
		def PeriodsList = Period.list().sort{};
		def taskPeriods = t.getPeriods();
		def test;

		for(s in PeriodsList) {

			try{
				test = taskPeriods.find { it.equals(s) }
			}
			catch(Exception e){
				println("GlpkData.getPeriodsFromTask() ${t.name}}: " + e.getMessage());
			}
			finally{
				println("task: ${t.name} / period: ${s.id}/ test: ${test} ");

				if(test) {
					result += "1 ";
				}
				else {
					result += "0 ";
				}
			}
		}
		return result;
	}
}

package app

static class Constants {
	
	
	static def glpkFileName = "assignment";
	static def glpkFileDirectory = "./solver";
	static def glpkFileExtMod = "mod";
	static def glpkFileExtOut = "out";
	static def glpkFileExtLog = "log";
	static def glpkFileExtDis = "dis";
	
	static def xmlFileDirectory = "./data/xml/";
	static def xmlFileExtMod = "xml";
	
	static def variableName = "x";
	static def objectiveName = "obj";
	static def paramPersonSkillsName = "personSkills";
	static def paramTaskSkillsName = "taskSkills";
	static def paramPersonPeriodsName = "personPeriods";
	static def paramTaskPeriodsName = "taskPeriods";
	
	static def lineSeparator = System.getProperty("line.separator");
	static def lineSeparatorGLPK = "\\n";
	static def doubleQuote = '"';
	static def commentOpen = '/*';
	static def commentClose = '*/';
	static def coma = ',';
	static def space = ' ';
	static def tab = '	';
	
	static def personSetName = 'PERSON';
	static def personSetIndex = 'p';
	static def taskSetName = 'TASK';
	static def taskSetIndex = 't';
	static def skillSetName = 'SKILL';
	static def skillSetIndex = 's';
	static def periodSetName = 'PERIOD';
	static def periodSetIndex = 'a';
	

	
}

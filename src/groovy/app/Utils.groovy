package app

import java.io.File;

static class Utils {

	static public String getFilePath(String dir, String name, String ext) {
		String s = "$dir/$name.$ext";
		println("Utils.getFilePath() : " + s);
		return s;
	}

	static public void cleanDirectory(String path) {
		File dir = new File(path);
		dir.eachFile{
			it.delete();
			println(it.name);
		}
	}

	static public File createFile(String directory,String name, String ext) {

		//First remove the old files if it exist
		cleanDirectory(directory);

		File file = null;
		try {
			file = new File(Utils.getFilePath(directory, name, ext));
			println("Utils.createFile() : " + file.toString());
		}
		catch(Exception e)
		{
			println("Utils.createFile() : " + e.getMessage());
		}
		finally
		{
			return file;
		}
	}

	static public void writeToFile(String directory,String name, String ext, String data) {

		File file = createFile(directory, name, ext);
		//File file = createFile(path);
		println("Utils.writeToFile() : "+file.getAbsolutePath());
		file.append(data);
	}

	



}

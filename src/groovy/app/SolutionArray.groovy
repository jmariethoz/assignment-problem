package app;

import assignment.Person;
import assignment.*;

class SolutionArray {
	int personCount = Person.count();
	int taskCount = Task.count();
	int periodCount = Period.count();

	def personList = Person.list().sort{};
	def TaskList = Task.list().sort{};
	def periodList = Period.list().sort{};

	//We have to add 1 header for each dimension
	def sol= new int[personCount][taskCount][periodCount];

	//Useless
	public void initArray(){
		(0..<sol.size()).each{i->
			(0..<sol[i].size()).each{j->
				(0..<sol[i][j].size()).each{k->
					sol[i][j][k] = 0;
					//println("initArray() : $i,$j,$k : ${sol[i][j][k]}")
				}
			}
		}
		println("initArray() : done")
		//initArrayHeaderDim(int dim);
	}

	/*
	 public void initArrayColumnHeader(){
	 Period.listOrderById{
	 (0..<sol[0].size()).each{col->
	 sol[0][col]=it;
	 println("initArrayColumnHeader() : 0,$col : ${sol[0][col]}")
	 };
	 }
	 }
	 */



	//FIXME convert id into array index -> get array index into ordered list of id (+-1)!!
	public void insertSolutionIntoArray(Integer personIndex, Integer taskIndex, Integer periodIndex, Integer solution) {
		sol[personIndex][taskIndex][periodIndex]=solution;
		if(solution)
		{
			println("insertSolutionIntoArray() : $personIndex,$taskIndex,$periodIndex");
		}
	}


	public void initArrayHeaderDim(int dim)
	{
		//TODO
	}

	//FIXME 2D -> 3D / param -> mode affichage
	String printArray3D(){
		String result= "";

		(0..<sol.size()).each{i->
			result+="${Constants.lineSeparator}"
			//result+="${Constants.lineSeparator} i($i)${Constants.tab}"
			(0..<sol[i].size()).each{j->
				result+="${Constants.lineSeparator}${Constants.lineSeparator}"
				//result+="${Constants.lineSeparator}${Constants.lineSeparator} j($j)${Constants.tab}"
				(0..<sol[i][j].size()).each{k->
					result+="${Constants.tab}${sol[i][j][k]}"
					//result+="${Constants.tab}k($k):${sol[i][j][k]}"

					if(sol[i][j][k]==1)
					{
						println("printArray() with solution : ${i},${j},${k} : ${sol[i][j][k]}");
					}
				}
			}
		}
		//println("printArray() result : $result");
		return result;
	}

	int[][] getArray2DPersonPeriod(){
		int[][] array2D = new int[personCount][periodCount];

		(0..<sol.size()).each{i->
			(0..<sol[i].size()).each{j->
				(0..<sol[i][j].size()).each{k->


					if(sol[i][j][k]==1){
						array2D[i][k] = j;
						println("getArray2DPersonPeriod() : ${i},${k}:${j}");
					}
				}
			}
		}
		return array2D;
	}

	int[][] getArray2DTaskPeriod(){
		int[][] array2D = new int[taskCount][periodCount];

		(0..<sol.size()).each{i->
			(0..<sol[i].size()).each{j->
				(0..<sol[i][j].size()).each{k->


					if(sol[i][j][k]==1){
						array2D[j][k] = i;
						println("getArray2DTaskPeriod() : ${j},${k}:${i}");
					}
				}
			}
		}
		return array2D;
	}

	def getArray2DPersonPeriodName()
	{
		def array = getArray2DPersonPeriod();
		def arrayName = new String[array.size()+1][array[0].size()+1];

		(0..<array.size()).each{i->
			(0..<array[i].size()).each{j->

				def itemIndex = array[i][j];
				if(itemIndex!=0){
					def t = Task.get(itemIndex+1).name;
					arrayName[i+1][j+1] = t;
					println("getArray2DPersonPeriodName() :  ${i+1},${j+1}:$t");
				}
			}
		}
	}

	def getArray2DTaskPeriodName()
	{
		def array = getArray2DTaskPeriod();
		def arrayName = new String[array.size()+1][array[0].size()+1];

		(0..<array.size()).each{i->
			(0..<array[i].size()).each{j->

				def itemIndex = array[i][j];
				if(itemIndex!=0){
					def p = Person.get(itemIndex+1).username
					arrayName[i+1][j+1] = p;
					println("getArray2DTaskPeriodName() :  ${i+1},${j+1}:$p");
				}

			}
		}

	}

	//TODO
	def getArrayHeaderColumn()
	{

	}

	//TODO
	def convertIdToIndex(def obj)
	{
		//if(obj.Class)

	}
}

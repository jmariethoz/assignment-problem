package system


static class SystemCommand{



	static public String execute(String cmd) {
		def proc="";
		try{
			proc = cmd.execute();	// Call *execute* on the string
			proc.waitFor();				// Wait for the command to finish

		}
		catch(Exception e) {
			println("SystemCommand.execute() : "+e.getMessage());
		}
		finally{
			// Obtain status and output
			def code = proc.exitValue();
			def err = proc.getErrorStream().getText();
			def input = proc.getInputStream().getText(); // *out* from the external program is *in* for groovy

			println "return code: ${code}";
			println "stderr: ${err}";
			println "stdout: ${input}";

			return cmd;
		}
	}
}

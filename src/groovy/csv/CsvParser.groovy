package csv


import java.lang.reflect.Array;

import assignment.Person;
import assignment.Task;
import assignment.Period;
import app.SolutionArray;
import app.Utils;
import app.Constants;

class CsvParser {

	def csvFile;
	def solArray = new SolutionArray();


	def CsvParse(String filepath) {

		csvFile = new File(filepath);
		int personIndex;
		int periodIndex;
		int taskIndex;

		//solArray.initArray();

		// split
		csvFile.splitEachLine(';'){fields ->

			Person p = Person.get(fields[0]);
			Task t = Task.get(fields[1]);
			Period a = Period.get(fields[2]);
			int solution = fields[3].toInteger();

			solArray.insertSolutionIntoArray(p.id.toInteger()-1, t.id.toInteger()-1, a.id.toInteger()-1, solution)
		}
	}

	//FIXME getParsingResults -> getParsingResultsPersons getParsingResultsTask
	def getParsingResults()
	{
		//def test = [[1,2],[3,4],[5,6]];
		//Utils.printArray2D(test);
		
		//println("getParsingResults() : solArray.getArray2DPersonPeriod()");
		def app = solArray.getArray2DPersonPeriod();
		//println("getParsingResults() : printArray2D()");
		//return printArray2D(app);
		
		//def atp = solArray.getArray2DTaskPeriod();
		//Utils.printArray2D(atp);
		
		//def appn = solArray.getArray2DPersonPeriodName();
		//Utils.printArray2D(appn);
		
		//def atpn = solArray.getArray2DTaskPeriodName();
		//Utils.printArray2D(atpn);
		
		
		
		
		return app;
		//return solArray.printArray3D();
		//return solArray;
	}
	
	def printArray2D(def array2d)
	{

		String result= "";
		(0..<array2d.size()).each{i->
			//println("${Constants.lineSeparator}");
			result+="${Constants.lineSeparator} i($i)${Constants.tab}"
			(0..<array2d[i].size()).each{j->
				//println("${Constants.lineSeparator}${Constants.lineSeparator}");
				result+="${Constants.tab}${array2d[i][j]}"

				//println("Utils.printArray2D() : ${i},${j} : ${sol[i][j][k]}");

			}
		}
		println("printArray2D(${array2d.getProperties()}) size : ${array2d.size()}x${array2d[0].size()}");
		println("printArray2D() result : ${Constants.lineSeparator}$result");
		return result;
	}
}

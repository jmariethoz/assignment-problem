package glpk

import csv.CsvParser;
import app.*;

class GlpkSolution {

	CsvParser csvParser;

	def getSolution() {
		def result = getCsvSolution(getCsvFilePath());
		println("GlpkSolution.getSolution()")
		return csvParser.printArray2D(result);
	}

	String getCsvFilePath() {
		String path = Utils.getFilePath(Constants.glpkFileDirectory, Constants.glpkFileName, Constants.glpkFileExtDis);
		println("getCsvFilePath() : $path");
		return path;
	}

	def getCsvSolution(String csvFilePath) {
		def result;
		csvParser = new CsvParser();
		csvParser.CsvParse(csvFilePath);
		result = csvParser.getParsingResults();
		println("GlpkSolution.getCsvSolution : create new csv parser and parse $csvFilePath")
		println("GlpkSolution.getCsvSolution : $result")
		
		return result;
	}
	
	
}

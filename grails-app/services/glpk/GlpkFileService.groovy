

package glpk

import system.*
import app.*;

class GlpkFileService{

	File gf;
	def fileName = Constants.glpkFileName;
	def fileDirectory = Constants.glpkFileDirectory;
	def fileExtMod = Constants.glpkFileExtMod;
	def fileExtOut = Constants.glpkFileExtOut;
	def fileExtLog = Constants.glpkFileExtLog;
	def fileExtDis = Constants.glpkFileExtDis;




	public String writeGLPKFile(String directory,String name, String ext) {
		String data = "";
		try {
			data = getGLPKFileContent();
			Utils.writeToFile(directory, name, ext, data);
			println("GlpkFileService.writeGLPKFile() : " + Utils.getFilePath(directory, name, ext));
		}
		catch(Exception e) {
			println("GlpkFileService.writeGLPKFile() : "+e.getMessage());
		}
		finally {
			return data;
		}
	}

	public String processGLPKFile() {
		/*http://en.wikibooks.org/wiki/GLPK/Interoperability*/

		println("GlpkFileService.processGLPKFile() : " + writeGLPKFile(fileDirectory, fileName, fileExtMod));
		println("GlpkFileService.processGLPKFile() command: glpsol --math ${Utils.getFilePath(fileDirectory, fileName, fileExtMod)} --output ${Utils.getFilePath(fileDirectory, fileName, fileExtOut)} --log ${Utils.getFilePath(fileDirectory, fileName, fileExtLog)} --display ${Utils.getFilePath(fileDirectory, fileName, fileExtDis)}");
		String cmd = SystemCommand.execute("glpsol --math ${Utils.getFilePath(fileDirectory, fileName, fileExtMod)} --output ${Utils.getFilePath(fileDirectory, fileName, fileExtOut)} --log ${Utils.getFilePath(fileDirectory, fileName, fileExtLog)} --display ${Utils.getFilePath(fileDirectory, fileName, fileExtDis)}");
		println(cmd);
		return cmd;
	}

	public String getGLPKFileContent() {

		GlpkModel model;
		GlpkCommand command;
		GlpkData data;

		try {
			model = new GlpkModel();
			command = new GlpkCommand();
			data = new GlpkData();
		}
		catch(Exception e) {
			println("GlpkFileService.getGLPKFileContent() : "+e.getMessage());
		}
		finally {
			return Constants.lineSeparator + model.toString() +
			Constants.lineSeparator + command.toString() +
			Constants.lineSeparator + data.toString();
		}
	}
}


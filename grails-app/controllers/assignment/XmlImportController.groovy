package assignment

import db.DataDeleter;
import system.*;
import xml.*;
import app.*;



class XmlImportController {
	XmlImportService xmlImportService

	String path = Constants.xmlFileDirectory;
	DataDeleter del = new DataDeleter();
	
	def importXml = {

		def filename = params.id.replace('###', '.')
		def file = new File( path + File.separatorChar +   filename )
		xmlImportService.importData(path + filename);

		println(path + filename)

		flash.message = "file "+filename+" imported";
		redirect( action:list )
	}

	def index = {
		redirect(action:list,params:params)
	}
	static transactional = true

	def allowedMethods = []

	def list = {
		def fileResourceInstanceList = []
		def f = new File(path)
		if( f.exists() ){
			f.eachFile(){ file->
				if( !file.isDirectory() )
					fileResourceInstanceList.add( file.name )
			}
		}
		[ fileResourceInstanceList: fileResourceInstanceList ]
	}

	def delete = {
		def filename = params.id.replace('###', '.')
		def file = new File( path + File.separatorChar +   filename )
		file.delete()
		flash.message = "file ${filename} removed"
		redirect( action:list )
	}

	def upload = {
		def f = request.getFile('fileUpload')
		if(!f.empty) {
			flash.message = 'Your file has been uploaded'
			new File(path).mkdirs()
			f.transferTo( new File( path + File.separatorChar + f.getOriginalFilename() ) )
		}
		else {
			flash.message = 'file cannot be empty'
		}
		redirect( action:list)
	}
	
	def deleteAll = {
		del.deleteAll();
		flash.message = 'All data removed'
		redirect( action:list)
	}

	def deletePersons = {
		del.deletePersons()
			flash.message = 'All persons removed'
			redirect( action:list)
	}
	
	def deleteTasks = {
		del.deletePersons()
			flash.message = 'All tasks removed'
			redirect( action:list)
		
	}

	def deleteSkills = {
		del.deleteSkills()
			flash.message = 'All skills removed'
			redirect( action:list)
		}
	

	
}

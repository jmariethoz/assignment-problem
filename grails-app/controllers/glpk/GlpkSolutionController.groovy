package glpk

import app.*;

class GlpkSolutionController {

	GlpkSolution glpkSolution = new GlpkSolution(); 
	
    def index = { 
		//println("GlpkSolutionController : "+ glpkSolution.getSolution());//.replace("${Constants.lineSeparator}","<br>");
		
		render glpkSolution.getSolution().replace("${Constants.lineSeparator}","<br>");
		}
}

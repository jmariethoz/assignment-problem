package assignment

class Task {

	String name

	static constraints = { name(blank:false, unique:true) }

	static hasMany = [skills: Skill,periods:Period]

	public String toString(){
		id
	}
}

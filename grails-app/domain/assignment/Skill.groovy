package assignment

class Skill {
	String name

	static constraints = { name(blank:false, unique:true) }

	static belongsTo = [Person, Task];
	static hasMany  = [person: Person, task:Task];


	public String toString(){
		id
	}
}

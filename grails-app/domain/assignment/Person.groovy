package assignment

class Person {
	String username
	String firstname
	String lastname
	String email

	static constraints = {
		firstname(blank:false)
		lastname(blank:false)
		email(email:true, blank:false)
		username( blank:false, unique:true)
	}

	static hasMany = [skills: Skill, periods:Period]

	public String toString(){
		//return "$firstname $lastname"
		return id.toString();
	}
	
	
}

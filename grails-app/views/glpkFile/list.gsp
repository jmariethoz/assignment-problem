<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="main" />
<title>GLPK file list</title>
</head>
<body>
	<div class="nav">
		<span class="menuButton"><a class="home" href="/assignment/">Home</a>
		</span>
	</div>




	<div>
		<h1>GLPK file list</h1>
		<g:if test="${flash.message}">
			<div class="message">
				${flash.message}
			</div>
		</g:if>
		<div id="success"></div>
		<div class="list">
			<table>
				<thead>
					<tr>
						<g:sortableColumn property="files" title="file" />
						<g:sortableColumn property="path" title="path" colspan="3" />
					</tr>
				</thead>
				<tbody>
					<g:each in="${fileResourceInstanceList}" status="i"
						var="fileResourceInstance">
						<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
							<td>
								${fileResourceInstance.decodeURL()}
							</td>
							<td><g:link action="download" id="${fileResourceInstance}"> download </g:link>
							</td>
							<td><g:link action="view" id="${fileResourceInstance}"> view </g:link>
							</td>
							<td><g:link action="delete"
									id="${fileResourceInstance.replace('.','###')}"
									onclick="return confirm('Do you want to delete?');"> delete </g:link>
							</td>
						</tr>
					</g:each>
				</tbody>
			</table>
		</div>
		<br />
		<div>
			<table>
				<tr>
					<td><g:link action="process"
							onclick="return confirm('Process glpk file again?');"> Process</g:link>
					</td>
				</tr>
			</table>
		</div>

	</div>
</body>
</html>
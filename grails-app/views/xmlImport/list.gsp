<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="main" />
<title>FileResource List</title>
</head>
<body>
	<div class="nav">
		<span class="menuButton"><a class="home" href="/assignment/">Home</a>
		</span>
	</div>
	<div class="body">

		<h1>File Upload:</h1>
		<br>

		<g:form method="post" enctype="multipart/form-data">
			<div class="dialog">
				<table>
					<tbody>
						<tr class="prop">
							<td valign="top" class="name"><label for="fileUpload">Upload:</label>
							</td>
							<td valign="top"
								class="value ${hasErrors(bean:fileResourceInstance,field:'upload','errors')}">

								<input type="file" id="fileUpload" name="fileUpload" /></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="buttons">
				<span class="button"><g:actionSubmit class="upload"
						value="Upload" action="upload" /> </span>
			</div>
		</g:form>

		<h1>XML file list</h1>
		<g:if test="${flash.message}">
			<div class="message">
				${flash.message}
			</div>
		</g:if>
		<div id="success"></div>
		<div class="list">
			<table>
				<thead>
					<tr>
						<g:sortableColumn property="files" title="file" />
						<g:sortableColumn property="path" title="path" colspan="4" />
					</tr>
				</thead>
				<tbody>
					<g:each in="${fileResourceInstanceList}" status="i"
						var="fileResourceInstance">
						<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
							<td>
								${fileResourceInstance.decodeURL()}
							</td>
							<td><input type="text"
								value="./data/xml/${fileResourceInstance.decodeURL()}"></input>
							</td>
							<td><g:link action="importXml"
									id="${fileResourceInstance.replace('.','###')}"
									onclick="return confirm('Import ${fileResourceInstance} ?');"> import </g:link>
							</td>
							<td><g:link action="delete"
									id="${fileResourceInstance.replace('.','###')}"
									onclick="return confirm('Delete ${fileResourceInstance} ?');"> delete </g:link>
							</td>
						</tr>
					</g:each>
				</tbody>
			</table>
		</div>
		<br />
		<div>
			<table>
				<tr>
					<td><g:link action="deleteAll"
							onclick="return confirm('Delete all data?');"> Delete all data</g:link>
					</td>
					<td><g:link action="deletePersons"
							onclick="return confirm('Delete all persons?');"> Delete all persons</g:link>
					</td>
					<td><g:link action="deleteTasks"
							onclick="return confirm('Delete all tasks?');"> Delete all tasks</g:link>
					</td>
					<td><g:link action="deleteSkills"
							onclick="return confirm('Delete all skills?');"> Delete all skills</g:link>
					</td>
				</tr>
			</table>
		</div>

	</div>
</body>
</html>
<html>
<head>
<title>Assignment problem application</title>
<meta name="layout" content="main" />
<style type="text/css" media="screen">
#nav {
	margin-top: 20px;
	margin-left: 30px;
	width: 228px;
	float: left;
}

.homePagePanel * {
	margin: 0px;
}

.homePagePanel .panelBody ul {
	list-style-type: none;
	margin-bottom: 10px;
}

.homePagePanel .panelBody h1 {
	text-transform: uppercase;
	font-size: 1.1em;
	margin-bottom: 10px;
}

.homePagePanel .panelBody {
	background: url(images/leftnav_midstretch.png) repeat-y top;
	margin: 0px;
	padding: 15px;
}

.homePagePanel .panelBtm {
	background: url(images/leftnav_btm.png) no-repeat top;
	height: 20px;
	margin: 0px;
}

.homePagePanel .panelTop {
	background: url(images/leftnav_top.png) no-repeat top;
	height: 11px;
	margin: 0px;
}

h2 {
	margin-top: 15px;
	margin-bottom: 15px;
	font-size: 1.2em;
}

#pageBody {
	margin-left: 280px;
	margin-right: 20px;
}
</style>
</head>
<body>
	<div id="nav">
		<div class="homePagePanel">
			<div class="panelTop"></div>
			<div class="panelBody">
				<h1>Application Status</h1>
				<ul>
					<li>App version: <g:meta name="app.version"></g:meta>
					</li>
					<li>Grails version: <g:meta name="app.grails.version"></g:meta>
					</li>
					<li>Groovy version: ${org.codehaus.groovy.runtime.InvokerHelper.getVersion()}
					</li>
					<li>JVM version: ${System.getProperty('java.version')}
					</li>
					<li>Controllers: ${grailsApplication.controllerClasses.size()}
					</li>
					<li>Domains: ${grailsApplication.domainClasses.size()}
					</li>
					<li>Services: ${grailsApplication.serviceClasses.size()}
					</li>
					<li>Tag Libraries: ${grailsApplication.tagLibClasses.size()}
					</li>
				</ul>
				<h1>Installed Plugins</h1>
				<ul>
					<g:set var="pluginManager"
						value="${applicationContext.getBean('pluginManager')}"></g:set>

					<g:each var="plugin" in="${pluginManager.allPlugins}">
						<li>
							${plugin.name} - ${plugin.version}
						</li>
					</g:each>

				</ul>
			</div>
			<div class="panelBtm"></div>
		</div>
	</div>
	<div id="pageBody">
		<h1>Assignment problem solving application using GLPK 4.45</h1>


		<div id=editDataControllerList>
			<h2>Edit the data you need :</h2>
			<table>
				<tr>
					<td class="controller"><g:link controller="person">
							Person
						</g:link>
					</td>
					<td>Edit person related data</td>
				</tr>
				<tr>
					<td class="controller"><g:link controller="task">
							Task
						</g:link>
					</td>
					<td>Edit task related data</td>
				</tr>
				<tr>
					<td class="controller"><g:link controller="skill">
							Skill
						</g:link>
					</td>
					<td>Edit skill related data. Persons have skills and
						tasks required skills.</td>
				</tr>
				<tr>
					<td class="controller"><g:link controller="period">
							Time period
						</g:link>
					</td>
					<td>Edit time period related data. Persons have availability and
						tasks need to be performed at a certain time.</td>
				</tr>
			</table>
		</div>

		<div id=importXmlDataControllerList>
			<h2>Import the data you need with xml files :</h2>
			<table>
				<tr>
					<td class="controller"><g:link controller="xmlImport">
							Xml import
						</g:link>
					</td>
					<td>Upload xml files and import the data in the database.</td>
				</tr>
			</table>
		</div>

		<div id=GlpkControllerList>
			<h2>Process the data with GLPK solver :</h2>
			<table>
				<tr>
					<td class="controller"><g:link controller="glpkFile">
							GLPK file
						</g:link>
					</td>
					<td>Create the GLPK model file with data from the database. The file is processed by the GLPK solver and the resulting files are can be downloaded or viewed from a list.</td>
				</tr>
			</table>
		</div>
		<div id="emptyDiv">
		<br><br><br>
		</div>
		<div id="controllerList" class="dialog">
			<h2>All Available Controllers:</h2>
			<ul>
				<g:each var="c"
					in="${grailsApplication.controllerClasses.sort { it.fullName } }">
					<li class="controller"><g:link
							controller="${c.logicalPropertyName}">
							${c.fullName}
						</g:link></li>
				</g:each>
			</ul>
		</div>
	</div>
</body>
</html>

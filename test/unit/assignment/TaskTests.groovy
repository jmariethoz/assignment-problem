package assignment

import grails.test.GrailsUnitTestCase;

class TaskTests extends GrailsUnitTestCase {
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testConstructorOK() {
		mockDomain(Task)
		Task t = new Task(name:'task1')
		t.validate()
		assertFalse t.hasErrors()
	}
}

package assignment

import java.util.jar.Attributes.Name;

import grails.test.*

class SkillTests extends GrailsUnitTestCase {
	protected void setUp() {
		super.setUp()
	}

	protected void tearDown() {
		super.tearDown()
	}

	
	public void testConstructor(){
		mockDomain(Skill)

		
		Skill c = [name:'competency1']
		assert c.validate()
		assert c.save()

		assert c.name == 'competency1';
		
	}
}
